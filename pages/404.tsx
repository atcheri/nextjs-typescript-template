import { withTranslation } from "next-i18next";
import Link from "next/link";

const NotFound = ({ t }) => (
  <div>
    <h1>{t("Oups")}</h1>
    <h2>{t("Page cannot be found")}</h2>
    <p>
      Go back to the{" "}
      <Link href="/">
        <a>{t("Homepage")}</a>
      </Link>
    </p>
  </div>
);

export default withTranslation("error")(NotFound);
