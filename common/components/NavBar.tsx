import Image from "next/image";
import Link from "next/link";

import { withTranslation } from "../../i18n";

const NavBar = ({ t }) => {
  return (
    <nav>
      <Image
        className="logo"
        src="/vercel.svg"
        alt="Logo"
        width={160}
        height={80}
      />
      <Link href="/">
        <a>{t("Homepage")}</a>
      </Link>
    </nav>
  );
};

export default withTranslation("navbar")(NavBar);
